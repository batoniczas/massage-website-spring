FROM openjdk:17-alpine
WORKDIR /app
COPY target/teratai-*.jar app.jar
ENTRYPOINT ["java", "-jar", "app.jar", "--spring.profiles.active=prod"]
