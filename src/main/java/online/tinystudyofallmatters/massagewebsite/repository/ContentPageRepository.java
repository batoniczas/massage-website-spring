package online.tinystudyofallmatters.massagewebsite.repository;

import online.tinystudyofallmatters.massagewebsite.entity.ContentPage;
import org.springframework.data.repository.CrudRepository;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ContentPageRepository extends CrudRepository<ContentPage, Integer> {
	@NonNull
	Optional<ContentPage> findByUrl(@NonNull String url);
}
