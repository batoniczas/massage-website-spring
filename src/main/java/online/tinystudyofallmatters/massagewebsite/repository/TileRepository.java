package online.tinystudyofallmatters.massagewebsite.repository;

import online.tinystudyofallmatters.massagewebsite.entity.Tile;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TileRepository extends CrudRepository<Tile, Integer> {
}
