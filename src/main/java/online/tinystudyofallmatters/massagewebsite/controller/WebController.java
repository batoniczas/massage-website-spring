package online.tinystudyofallmatters.massagewebsite.controller;

import jakarta.persistence.EntityNotFoundException;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import online.tinystudyofallmatters.massagewebsite.entity.ContentPage;
import online.tinystudyofallmatters.massagewebsite.entity.Tile;
import online.tinystudyofallmatters.massagewebsite.repository.ContentPageRepository;
import online.tinystudyofallmatters.massagewebsite.repository.TileRepository;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

@Slf4j
@Controller
public class WebController {
	private final ContentPageRepository contentPageRepository;
	private final TileRepository tileRepository;

	public WebController(final ContentPageRepository contentPageRepository, final TileRepository tileRepository) {
		this.contentPageRepository = contentPageRepository;
		this.tileRepository = tileRepository;
	}

	@GetMapping("/**")
	public String page(@NonNull final Model model, @NonNull final HttpServletRequest httpServletRequest) {
		final String url = httpServletRequest.getRequestURI();
		if (url.equals("/")) {
			return mainPage(model);
		}
		final ContentPage contentPage = contentPageRepository.findByUrl(url)
				.orElseThrow(() -> new EntityNotFoundException(url));
		model.addAttribute("title", contentPage.getTitle());
		model.addAttribute("text", contentPage.getText());
		model.addAttribute("photo_url", contentPage.getPhotoUrl());
		model.addAttribute("alt", contentPage.getAlt());
		return "content-page";
	}

	@GetMapping("favicon.ico")
	public ResponseEntity<Resource> faviconIco() throws IOException {
		return getResourceResponseEntity("favicon.ico", "image/x-icon");
	}

	@GetMapping("style.css")
	public ResponseEntity<Resource> styleCss() throws IOException {
		return getResourceResponseEntity("style.css", "text/css");
	}

	@GetMapping("script.js")
	public ResponseEntity<Resource> scriptJs() throws IOException {
		return getResourceResponseEntity("script.js", "application/javascript");
	}

	@GetMapping
	public String mainPage(@NonNull final Model model) {
		model.addAttribute("invite", "Zapraszam!");
		model.addAttribute("introduce", "Jestem pasjonatą masażu");
		model.addAttribute("text1", "Oferta");
		model.addAttribute("text2", "Moje usługi");
		model.addAttribute("text", "Moje priorytety skupiają się na zapewnieniu klientom niezrównanego relaksu i dobrostanu poprzez spersonalizowane podejście do ich potrzeb. Świadczę profesjonalne usługi masażu dostosowane do preferowanego przez klienta miejsca. Niezależnie od tego, czy klient woli masaż w domu, hotelu czy biurze, elastycznie dopasowuję się do jego oczekiwań, zapewniając relaksacyjne doznania masażu w komfortowym otoczeniu, które mu odpowiada.");
		model.addAttribute("under", "Poczuj harmonię i głęboki relaks w profesjonalnym masażu dostosowanym do Twoich potrzeb – zadbaj o swój dobrostan już teraz!");
		model.addAttribute("tiles", getMainPageTiles());
		return "main";
	}

	@NonNull
	private List<Tile> getMainPageTiles() {
		return Stream.of(
						tileRepository.findById(1),
						tileRepository.findById(2),
						tileRepository.findById(3),
						tileRepository.findById(4),
						tileRepository.findById(5),
						tileRepository.findById(6))
				.map(tile -> tile.orElse(null))
				.filter(Objects::nonNull)
				.toList();
	}

	@GetMapping("/kontakt")
	public String kontakt() {
		return "contact";
	}

	@GetMapping("/cennik")
	public String cennik() {
		return "cennik";
	}

	@GetMapping("/pakiety")
	public String pakiety() {
		return "pakiety";
	}

	@NonNull
	private ResponseEntity<Resource> getResourceResponseEntity(@NonNull final String name, @NonNull final String contentType) throws IOException {
		final Resource resource = new ClassPathResource(String.format("static/%s", name));
		final HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add(HttpHeaders.CONTENT_DISPOSITION, String.format("attachment; filename=%s", name));
		httpHeaders.add(HttpHeaders.CONTENT_TYPE, contentType);
		return ResponseEntity.ok()
				.headers(httpHeaders)
				.contentLength(resource.contentLength())
				.body(resource);
	}
	//Szuka zasobu, uniwersalne dla styli i skryptów, żeby nie duplikować bytu
}
