package online.tinystudyofallmatters.massagewebsite.controller;

import jakarta.persistence.EntityNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler {
	@ExceptionHandler(EntityNotFoundException.class)
	public ResponseEntity<Object> handle(final EntityNotFoundException exception) {
		return ResponseEntity.status(HttpStatus.NOT_FOUND)
				.body(exception.getMessage());
	}
}

//exception handler
//jeśli gdzieś w całym springu poleci wyjątek EntityNotFoundException to zamiast nam popsuć backend
// i wyrzucić jakieś 500, ta metoda się odpali i wyciszy wyjątek, zamiast tego zrobi "ładną" odpowiedź,
// w tym wypadku 404
