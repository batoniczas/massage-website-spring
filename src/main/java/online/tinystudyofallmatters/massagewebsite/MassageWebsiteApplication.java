package online.tinystudyofallmatters.massagewebsite;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MassageWebsiteApplication {
	public static void main(final String[] args) {
		SpringApplication.run(MassageWebsiteApplication.class, args);
	}
}
