package online.tinystudyofallmatters.massagewebsite.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.With;
import org.springframework.lang.Nullable;

import java.util.Objects;

@With
@Getter
@Setter
@Entity
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ContentPage {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String title;
	private String photoUrl;
	private String url;
	private String text;
	private String alt;

	@Override
	public boolean equals(@Nullable final Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof final ContentPage that)) {
			return false;
		}
		return getId() == that.getId()
				&& Objects.equals(getTitle(), that.getTitle())
				&& Objects.equals(getPhotoUrl(), that.getPhotoUrl())
				&& Objects.equals(getUrl(), that.getUrl())
				&& Objects.equals(getText(), that.getText())
				&& Objects.equals(getAlt(), that.getAlt());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getId(), getTitle(), getPhotoUrl(), getUrl(), getText(), getAlt());
	}
}
