package online.tinystudyofallmatters.massagewebsite.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.With;
import org.springframework.lang.Nullable;

import java.util.Objects;

@With
@Setter
@Getter
@Entity
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Tile {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String title;
	private String text;
	private String photoUrl;
	private String url;
	private String alt;

	@Override
	public boolean equals(@Nullable final Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof final Tile tile)) {
			return false;
		}
		return getId() == tile.getId()
				&& Objects.equals(getTitle(), tile.getTitle())
				&& Objects.equals(getText(), tile.getText())
				&& Objects.equals(getPhotoUrl(), tile.getPhotoUrl())
				&& Objects.equals(getUrl(), tile.getUrl())
				&& Objects.equals(getAlt(), tile.getAlt());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getId(), getTitle(), getText(), getPhotoUrl(), getUrl(), getAlt());
	}
}
