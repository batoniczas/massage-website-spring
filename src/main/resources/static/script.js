document.addEventListener("DOMContentLoaded", () => {
    const observer = new IntersectionObserver(entries => entries.forEach(entry => {
        if (entry.isIntersecting) {
            entry.target.classList.add('animate');
        } else {
            entry.target.classList.remove('animate');
        }
        console.log(entry)
    }), {
        threshold: 1.0
    });

    document.querySelectorAll('.image-container').forEach(el => observer.observe(el));
});
