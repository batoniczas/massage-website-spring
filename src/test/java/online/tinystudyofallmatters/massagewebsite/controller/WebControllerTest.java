package online.tinystudyofallmatters.massagewebsite.controller;

import online.tinystudyofallmatters.massagewebsite.entity.ContentPage;
import online.tinystudyofallmatters.massagewebsite.entity.Tile;
import online.tinystudyofallmatters.massagewebsite.repository.ContentPageRepository;
import online.tinystudyofallmatters.massagewebsite.repository.TileRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpHeaders;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@WebMvcTest(WebController.class)
class WebControllerTest {
	@Autowired
	private MockMvc mockMvc;
	@MockBean
	private ContentPageRepository contentPageRepository;
	@MockBean
	private TileRepository tileRepository;

	@Test
	void mainPage() throws Exception {
		final Tile tile1 = new Tile().withId(1).withTitle("Test1").withAlt("test2").withUrl("test1").withText("test1");
		final Tile tile2 = new Tile().withId(2).withTitle("Test2").withAlt("test2").withUrl("test2").withText("test2");
		final Tile tile3 = new Tile().withId(3).withTitle("Test3").withAlt("test3").withUrl("test3").withText("test3");
		final Tile tile4 = new Tile().withId(4).withTitle("Test4").withAlt("test4").withUrl("test4").withText("test4");
		final Tile tile5 = new Tile().withId(5).withTitle("Test5").withAlt("test5").withUrl("test5").withText("test5");
		final Tile tile6 = new Tile().withId(6).withTitle("Test6").withAlt("test6").withUrl("test6").withText("test6");

		when(tileRepository.findById(1)).thenReturn(Optional.of(tile1));
		when(tileRepository.findById(2)).thenReturn(Optional.of(tile2));
		when(tileRepository.findById(3)).thenReturn(Optional.of(tile3));
		when(tileRepository.findById(4)).thenReturn(Optional.of(tile4));
		when(tileRepository.findById(5)).thenReturn(Optional.of(tile5));
		when(tileRepository.findById(6)).thenReturn(Optional.of(tile6));

		mockMvc.perform(get("/")).andExpect(status().isOk())
				.andExpect(view().name("main"))
				.andExpect(model().attribute("invite", "Zapraszam!"))
				.andExpect(model().attribute("tiles", List.of(tile1, tile2, tile3, tile4, tile5, tile6)));
	}

	@Test
	void testContentPage() throws Exception {
		final ContentPage contentPage = new ContentPage()
				.withTitle("Test")
				.withUrl("/test")
				.withId(1)
				.withText("test")
				.withPhotoUrl("test")
				.withAlt("test");
		when(contentPageRepository.findByUrl("/test"))
				.thenReturn(Optional.of(contentPage));
		mockMvc.perform(get("/test"))
				.andExpect(status().isOk())
				.andExpect(view().name("content-page"))
				.andExpect(model().attribute("title", "Test"))
				.andExpect(model().attribute("text", "test"))
				.andExpect(model().attribute("photo_url", "test"))
				.andExpect(model().attribute("alt", "test"));
	}

	@Test
	void testContentPageNotFound() throws Exception {
		when(contentPageRepository.findByUrl("/test"))
				.thenReturn(Optional.empty());
		mockMvc.perform(get("/test"))
				.andExpect(status().isNotFound());
	}

	@Test
	void testContact() throws Exception {
		mockMvc.perform(get("/kontakt"))
				.andExpect(status().isOk())
				.andExpect(view().name("contact"));
	}

	@Test
	void testCennik() throws Exception {
		mockMvc.perform(get("/cennik"))
				.andExpect(status().isOk())
				.andExpect(view().name("cennik"));
	}

	@Test
	void testScript() throws Exception {
		mockMvc.perform(get("/script.js"))
				.andExpect(status().isOk())
				.andExpect(header().string(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=script.js"))
				.andExpect(header().string(HttpHeaders.CONTENT_TYPE, "application/javascript"))
				.andExpect(content().bytes(new ClassPathResource("static/script.js").getInputStream().readAllBytes()));
	}

	@Test
	void testStyle() throws Exception {
		mockMvc.perform(get("/style.css"))
				.andExpect(status().isOk())
				.andExpect(header().string(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=style.css"))
				.andExpect(header().string(HttpHeaders.CONTENT_TYPE, "text/css"))
				.andExpect(content().bytes(new ClassPathResource("static/style.css").getInputStream().readAllBytes()));
	}

	@Test
	void testFavicon() throws Exception {
		mockMvc.perform(get("/favicon.ico"))
				.andExpect(status().isOk())
				.andExpect(header().string(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=favicon.ico"))
				.andExpect(header().string(HttpHeaders.CONTENT_TYPE, "image/x-icon"))
				.andExpect(content().bytes(new ClassPathResource("static/favicon.ico").getInputStream().readAllBytes()));
	}
}
