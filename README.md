# TERATAI.PL

#### The website for the Teratai.pl brand, specializing in exotic massages, was created not only using my programming knowledge but also drawing on my experience in marketing consulting and business strategy for young companies.

#### Technologies: Spring Boot, Java, HTML, Thymeleaf, JPA

- All content in database
- Modern tiles displaying offered services
- Project division into components
- Simple, minimalist, and clear website
- Subpages such as price list, massage descriptions, about me
- Attention to detail and clarity of code
